*Informationen*
Dieser Bot wurde von @niwla23 und der FFF Bot AG geschrieben. Bei Fragen, Kritik und Vorschlägen bitte einen
Issue auf GitLab erstellen oder @niwla23 anschreiben.

`Source Code`:  https://gitlab.com/fff-bots/fff-profile-picture-bot


*Datenschutz*

Der Schutz persönlicher Daten ist uns wichtig. Daher ist dieser Bot komplett Open-Source,
das heißt du kannst dir ansehen, was er tut.

Wir speichern keinerlei personenbezogene Daten dauerhaft auf dem Server. Das aktuelle Profilbild wird an den
Server gesendet, dort bearbeitet und nach der bearbeitung wieder gelöscht. Ebenso das fertige Bild wird nur
für wenige Sekunden gespeichert. Diese kurzzeitige Speicherung dient der Funktionalität des Bots, ohne diese
Funktionen könnte der Bot nicht funktionieren.

__Anonyme Nutzungsstatistiken__
Bei jeder Benutzung des Bots wird folgendes gespeichert:
`Jemand hat den Profilbildgenerator genutzt` sowie den Zeitpunkt der Benutzung

Der Server steht in Deutschland.